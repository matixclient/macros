package de.paxii.clarinet.macros;

import de.paxii.clarinet.Wrapper;
import de.paxii.clarinet.event.EventHandler;
import de.paxii.clarinet.event.events.game.KeyPressedEvent;

import java.util.HashMap;

import lombok.Getter;

/**
 * Created by Lars on 13.09.2015.
 */
public class MacroManager {
  @Getter
  private HashMap<Integer, Macro> macroMap;

  public MacroManager() {
    this.macroMap = new HashMap<>();

    Wrapper.getEventManager().register(this);
  }

  public void addMacro(Macro macro) {
    this.macroMap.put(macro.getKeyCode(), macro);
  }

  public void removeMacro(int keyCode) {
    this.macroMap.remove(keyCode);
  }

  @EventHandler
  public void onKeyPress(KeyPressedEvent event) {
    int keyCode = event.getKey();

    if (this.macroMap.containsKey(keyCode)) {
      Macro macro = this.macroMap.get(keyCode);
      macro.executeActions();
    }
  }
}
