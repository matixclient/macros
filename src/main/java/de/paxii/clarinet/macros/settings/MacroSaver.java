package de.paxii.clarinet.macros.settings;

import com.google.common.base.Charsets;
import com.google.common.hash.Hashing;

import de.paxii.clarinet.macros.Macro;
import de.paxii.clarinet.module.external.ModuleMacros;
import de.paxii.clarinet.util.file.FileService;
import de.paxii.clarinet.util.settings.ClientSettings;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;

/**
 * Created by Lars on 13.09.2015.
 */
public class MacroSaver {
  private File macroFolder;

  public MacroSaver() {
    this.macroFolder = new File(ClientSettings.getClientFolderPath().getValue(), "macros");
    this.macroFolder.mkdirs();

    this.loadMacros();
  }

  private void loadMacros() {
    File[] macroFiles = this.macroFolder.listFiles(file -> file.getName().endsWith(".json"));

    if (macroFiles == null) {
      return;
    }

    Arrays.stream(macroFiles).forEach(macroFile -> {
      try {
        Macro macro = FileService.getFileContents(macroFile, Macro.class);
        ModuleMacros.getMacroManager().addMacro(macro);
      } catch (IOException e) {
        e.printStackTrace();
      }
    });
  }

  public void saveMacro(Macro macro) {
    // TODO: Remove in future versions. This only ensures that legacy files are being removed instead of duplicated
    this.removeMacro(macro);

    File macroFile = this.getMacroFile(macro);
    try {
      FileService.setFileContentsAsJson(macroFile, macro);
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  public void removeMacro(Macro macro) {
    File macroFile = this.getMacroFile(macro);
    File legacyFile = new File(this.macroFolder, String.format("%s.json", macro.getName()));
    macroFile.delete();
    legacyFile.delete();
  }

  private File getMacroFile(Macro macro) {
    String fileName = Hashing.sha1().hashString(macro.getName(), Charsets.UTF_8).toString();

    return new File(this.macroFolder, String.format("%s.json", fileName));
  }
}
