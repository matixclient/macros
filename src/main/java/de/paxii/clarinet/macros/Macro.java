package de.paxii.clarinet.macros;

import de.paxii.clarinet.module.external.ModuleMacros;

import java.util.ArrayList;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by Lars on 13.09.2015.
 */
public class Macro implements Comparable<Macro> {
  @Getter
  @Setter
  private String name;

  @Getter
  private int keyCode;

  @Getter
  @Setter
  private ArrayList<MacroAction> macroList;

  public Macro(String name, int keyCode) {
    this.name = name;
    this.keyCode = keyCode;
    this.macroList = new ArrayList<>();
  }

  public void addAction(MacroAction macroAction) {
    this.macroList.add(macroAction);
  }

  public void executeActions() {
    this.macroList.forEach(MacroAction::executeAction);
  }

  public void setKeyCode(int keyCode) {
    ModuleMacros.getMacroManager().removeMacro(this.keyCode);

    this.keyCode = keyCode;
    ModuleMacros.getMacroManager().addMacro(this);
  }

  @Override
  public int compareTo(Macro macro) {
    return this.getName().compareToIgnoreCase(macro.getName());
  }
}
