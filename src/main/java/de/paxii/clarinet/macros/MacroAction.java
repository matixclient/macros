package de.paxii.clarinet.macros;

import de.paxii.clarinet.macros.actions.IAction;
import de.paxii.clarinet.module.external.ModuleMacros;

import lombok.Value;

/**
 * Created by Lars on 13.09.2015.
 */
@Value
public class MacroAction {

  private String actionName;

  private String[] actionParameters;

  public MacroAction(String actionName, String[] actionParameters) {
    this.actionName = actionName;
    this.actionParameters = actionParameters;
  }

  public void executeAction() {
    IAction iAction = ModuleMacros.getActionManager().getActionByName(this.actionName);

    if (iAction != null) {
      iAction.executeAction(this.actionParameters);
    }
  }
}
