package de.paxii.clarinet.macros.actions.actionlist;

import de.paxii.clarinet.macros.actions.IAction;

import net.minecraft.client.settings.KeyBinding;

import java.util.Arrays;

/**
 * Created by Lars on 13.09.2015.
 */
public class ActionKey implements IAction {

  @Override
  public String getName() {
    return "key";
  }

  @Override
  public void executeAction(String[] args) {
    Arrays.stream(args)
            .map(Integer::parseInt)
            .filter(keyCode -> keyCode != -1)
            .forEach(KeyBinding::onTick);
  }
}
