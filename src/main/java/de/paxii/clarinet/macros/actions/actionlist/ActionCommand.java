package de.paxii.clarinet.macros.actions.actionlist;

import de.paxii.clarinet.Wrapper;
import de.paxii.clarinet.event.events.player.PlayerSendChatMessageEvent;
import de.paxii.clarinet.macros.actions.IAction;
import de.paxii.clarinet.util.settings.ClientSettings;

import net.minecraft.network.play.client.CPacketChatMessage;

/**
 * Created by Lars on 13.09.2015.
 */
public class ActionCommand implements IAction {
  @Override
  public String getName() {
    return "command";
  }

  @Override
  public void executeAction(String[] args) {
    String chatMessage = String.join(" ", args);
    String prefix = ClientSettings.getValue("client.prefix", String.class);

    Wrapper.getConsole().onChatMessage(new PlayerSendChatMessageEvent(
            new CPacketChatMessage(chatMessage.startsWith(prefix) ? chatMessage : prefix + chatMessage)
    ));
  }
}
