package de.paxii.clarinet.macros.actions;

import de.paxii.clarinet.macros.actions.actionlist.ActionChat;
import de.paxii.clarinet.macros.actions.actionlist.ActionCommand;
import de.paxii.clarinet.macros.actions.actionlist.ActionKey;

import java.util.ArrayList;
import java.util.stream.Stream;

/**
 * Created by Lars on 13.09.2015.
 */
public class ActionManager {
  private final ArrayList<IAction> actionList;

  public ActionManager() {
    this.actionList = new ArrayList<>();

    Stream.of(
            new ActionChat(),
            new ActionCommand(),
            new ActionKey()
    ).forEach(this::addAction);
  }

  public void addAction(IAction iAction) {
    if (!this.actionList.contains(iAction)) {
      this.actionList.add(iAction);
    }
  }

  public void removeAction(IAction iAction) {
    this.actionList.remove(iAction);
  }

  public IAction getActionByName(String actionName) {
    return this.actionList.stream()
            .filter(action -> action.getName().equalsIgnoreCase(actionName))
            .findFirst().orElse(null);
  }
}
