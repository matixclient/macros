package de.paxii.clarinet.macros.actions;

/**
 * Created by Lars on 13.09.2015.
 */
public interface IAction {

  String getName();
  void executeAction(String[] args);

}
