package de.paxii.clarinet.macros.actions.actionlist;

import de.paxii.clarinet.macros.actions.IAction;
import de.paxii.clarinet.util.chat.Chat;

/**
 * Created by Lars on 13.09.2015.
 */
public class ActionChat implements IAction {

  @Override
  public String getName() {
    return "chat";
  }

  @Override
  public void executeAction(String[] args) {
    Chat.sendChatMessage(String.join(" ", args).trim());
  }
}
