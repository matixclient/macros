package de.paxii.clarinet.macros.gui;

import de.paxii.clarinet.Wrapper;
import de.paxii.clarinet.macros.Macro;
import de.paxii.clarinet.macros.MacroAction;
import de.paxii.clarinet.module.external.ModuleMacros;
import de.paxii.clarinet.util.notifications.NotificationPriority;

import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.GuiTextField;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by Lars on 13.09.2015.
 */
public class GuiCreateMacro extends GuiScreen {

  private final GuiScreen parentScreen;

  @Getter
  @Setter
  private Macro macro;

  private GuiButton cancelButton;
  private GuiButton removeButton;
  private GuiButton saveButton;
  private GuiButton addActionButton;

  private GuiTextField nameText;
  private ArrayList<GuiTextField> textFields;

  public GuiCreateMacro(GuiScreen parentScreen) {
    this(parentScreen, new Macro("", -1));
  }

  public GuiCreateMacro(GuiScreen parentScreen, Macro macro) {
    this.parentScreen = parentScreen;

    this.macro = macro;
    this.textFields = new ArrayList<>();
  }

  @Override
  public void initGui() {
    Stream.of(
            this.addActionButton = new GuiButton(2, this.width / 2 + 145, this.height - 25, 100, 20, "Add Action"),
            this.saveButton = new GuiButton(1, this.width / 2 + 25, this.height - 25, 100, 20, "Save"),
            this.removeButton = new GuiButton(3, this.width / 2 - 85, this.height - 25, 100, 20, "Remove"),
            this.cancelButton = new GuiButton(0, this.width / 2 - 205, this.height - 25, 100, 20, "Cancel")
    ).forEach(this.buttonList::add);

    this.nameText = new GuiTextField(3, Wrapper.getFontRenderer(), this.width / 2 - 50, 40, 100, 20);

    if (this.macro.getName().length() != 0 && this.nameText.getText().length() == 0) {
      this.nameText.setText(this.macro.getName());
    }

    if (this.textFields.isEmpty()) {
      if (!this.macro.getMacroList().isEmpty()) {
        this.macro.getMacroList().stream()
                .map(macroAction -> {
                  int index = this.macro.getMacroList().indexOf(macroAction);
                  GuiTextField textField = new GuiTextField(index + 1000, Wrapper.getFontRenderer(), this.width / 3, 80 + 30 * index, this.width / 3, 20);
                  textField.setMaxStringLength(1000);
                  textField.setText(macroAction.getActionName() + ":" + String.join(" ", macroAction.getActionParameters()).trim());

                  return textField;
                })
                .forEach(this.textFields::add);
      } else {
        GuiTextField textField = new GuiTextField(1000, Wrapper.getFontRenderer(), this.width / 3, 110, this.width / 3, 20);
        textField.setMaxStringLength(1000);
        this.textFields.add(textField);
      }
    } else {
      this.textFields.forEach(textField -> {
        int index = this.textFields.indexOf(textField);
        textField.x = this.width / 3;
        textField.y = 100 + 30 * index;
        try {
          Field width = GuiTextField.class.getDeclaredField("width");
          width.setAccessible(true);
          width.set(textField, this.width / 3);
        } catch (ReflectiveOperationException e) {
          e.printStackTrace();
        }
      });
    }
  }

  @Override
  public void drawScreen(int mouseX, int mouseY, float partialTicks) {
    this.drawDefaultBackground();

    this.drawCenteredString(Wrapper.getFontRenderer(), "Macro", this.width / 2, 10, 0xffffff);
    this.drawCenteredString(Wrapper.getFontRenderer(), "Macro Name:", this.width / 2, 30, 0xffffff);
    this.nameText.drawTextBox();
    this.drawCenteredString(Wrapper.getFontRenderer(), "Actions:", this.width / 2, 80, 0xffffff);
    this.textFields.forEach(GuiTextField::drawTextBox);

    super.drawScreen(mouseX, mouseY, partialTicks);
  }

  @Override
  public void updateScreen() {
    this.nameText.updateCursorCounter();
    this.textFields.forEach(GuiTextField::updateCursorCounter);

    super.updateScreen();
  }

  @Override
  protected void actionPerformed(GuiButton button) {
    if (button.id == this.cancelButton.id) {
      Wrapper.getMinecraft().displayGuiScreen(this.parentScreen);
    } else if (button.id == this.saveButton.id) {
      this.macro.setName(this.nameText.getText());
      this.macro.setMacroList(this.textFields.stream()
              .map(GuiTextField::getText)
              .filter(text -> text.contains(":"))
              .map(text -> text.split(":"))
              .map(split -> {
                String actionName = split[0];
                String actionParameters = String.join(":", Arrays.stream(split).skip(1).toArray(String[]::new));

                return new MacroAction(actionName, actionParameters.split(" "));
              })
              .collect(Collectors.toCollection(ArrayList::new)));

      if (this.textFields.stream().filter(textField -> !textField.getText().contains(":")).count() > 0) {
        Wrapper.getClient().getNotificationManager().addNotification(
                "One of the actions was invalid and wasn't saved!", NotificationPriority.DANGER
        );
      }
      ModuleMacros.getInstance().getMacroSaver().saveMacro(this.macro);
      ModuleMacros.getMacroManager().addMacro(this.macro);
      Wrapper.getMinecraft().displayGuiScreen(new GuiMacros(null));
    } else if (button.id == this.addActionButton.id) {
      this.macro.setName(this.nameText.getText());

      if (this.textFields.size() < 10) {
        GuiTextField textField = new GuiTextField((this.textFields.size() + 1000), Wrapper.getFontRenderer(), this.width / 3, 80 + 30 * this.textFields.size(), this.width / 3, 20);
        textField.setMaxStringLength(1000);
        this.textFields.add(textField);
      }
    } else if (button.id == this.removeButton.id) {
      ModuleMacros.getInstance().getMacroSaver().removeMacro(this.macro);
      ModuleMacros.getMacroManager().removeMacro(this.macro.getKeyCode());
      Wrapper.getMinecraft().displayGuiScreen(new GuiMacros(null));
    }
  }

  @Override
  protected void mouseClicked(int mouseX, int mouseY, int key) throws IOException {
    this.nameText.mouseClicked(mouseX, mouseY, key);
    this.textFields.forEach(textField -> textField.mouseClicked(mouseX, mouseY, key));

    super.mouseClicked(mouseX, mouseY, key);
  }

  @Override
  protected void keyTyped(char typedChar, int keyCode) throws IOException {
    this.nameText.textboxKeyTyped(typedChar, keyCode);
    this.textFields.forEach(textField -> textField.textboxKeyTyped(typedChar, keyCode));

    super.keyTyped(typedChar, keyCode);
  }
}
