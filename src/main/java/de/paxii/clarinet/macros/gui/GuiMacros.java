package de.paxii.clarinet.macros.gui;

import de.paxii.clarinet.Wrapper;

import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;

import java.io.IOException;
import java.util.stream.Stream;

import lombok.Getter;
import lombok.Setter;

public class GuiMacros extends GuiScreen {

  @Setter
  private GuiScreen parentScreen;

  @Getter
  @Setter
  private boolean shouldListen;

  private GuiKeybindsList keybindsList;
  @Getter
  @Setter
  private GuiListEntryModuleKey selectedButton;

  @Getter
  @Setter
  private GuiListEntryModuleKey pressedButton;

  @Getter
  private int pressedKey;

  private GuiButton doneButton;
  private GuiButton addButton;
  private GuiButton keyButton;
  private GuiButton editButton;

  public GuiMacros(GuiScreen parentScreen) {
    this.parentScreen = parentScreen;
  }

  @Override
  public void initGui() {
    this.keybindsList = new GuiKeybindsList(this);

    Stream.of(
            this.doneButton = new GuiButton(0, this.width / 2 + 145, this.height - 25, 100, 20, "Done"),
            this.addButton = new GuiButton(1, this.width / 2 + 25, this.height - 25, 100, 20, "Add"),
            this.keyButton = new GuiButton(2, this.width / 2 - 85, this.height - 25, 100, 20, "KeyBind"),
            this.editButton = new GuiButton(3, this.width / 2 - 205, this.height - 25, 100, 20, "Edit")
    ).forEach(this.buttonList::add);
  }

  @Override
  protected void keyTyped(char typedChar, int keyCode) {
    if (keyCode == 1) {
      if (this.isShouldListen()) {
        this.getPressedButton().getModule().setKeyCode(-1);

        this.setShouldListen(false);
        this.setPressedButton(null);
      } else {
        Wrapper.getMinecraft().displayGuiScreen(this.parentScreen);
      }
    }

    if (this.isShouldListen()) {
      if (this.getPressedButton() != null) {
        this.getPressedButton().getModule().setKeyCode(keyCode);
      }

      this.setShouldListen(false);
      this.pressedKey = keyCode;
    }
  }

  @Override
  public void handleMouseInput() throws IOException {
    super.handleMouseInput();
    this.keybindsList.handleMouseInput();
  }

  @Override
  protected void actionPerformed(GuiButton button) {
    if (button.id == this.doneButton.id) {
      Wrapper.getMinecraft().displayGuiScreen(this.parentScreen);
    } else if (button.id == this.addButton.id) {
      Wrapper.getMinecraft().displayGuiScreen(new GuiCreateMacro(this));
    } else if (button.id == this.editButton.id) {
      if (this.getPressedButton() != null) {
        Wrapper.getMinecraft().displayGuiScreen(new GuiCreateMacro(this, this.getPressedButton().getModule()));
      }
    } else if (button.id == this.keyButton.id) {
      this.setShouldListen(!this.isShouldListen());
    }
  }

  @Override
  protected void mouseClicked(int mouseX, int mouseY, int mouseButton)
          throws IOException {
    this.keybindsList.mouseClicked(mouseX, mouseY, mouseButton);

    super.mouseClicked(mouseX, mouseY, mouseButton);
  }

  @Override
  protected void mouseReleased(int mouseX, int mouseY, int state) {
    this.keybindsList.mouseReleased(mouseX, mouseY, state);
    super.mouseReleased(mouseX, mouseY, state);
  }

  @Override
  public void drawScreen(int mouseX, int mouseY, float partialTicks) {
    this.drawDefaultBackground();

    if (this.keybindsList != null) {
      this.keybindsList.drawScreen(mouseX, mouseY, partialTicks);
    }

    this.drawCenteredString(Wrapper.getFontRenderer(), "Macros", this.width / 2, 8, 16777215);

    super.drawScreen(mouseX, mouseY, partialTicks);
  }
}
