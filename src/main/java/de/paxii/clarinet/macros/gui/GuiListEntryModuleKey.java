package de.paxii.clarinet.macros.gui;

import de.paxii.clarinet.Wrapper;
import de.paxii.clarinet.macros.Macro;
import de.paxii.clarinet.util.render.GuiMethods;

import net.minecraft.client.gui.GuiListExtended;
import net.minecraft.client.gui.GuiScreen;

import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.GL11;

import lombok.Getter;

public class GuiListEntryModuleKey implements GuiListExtended.IGuiListEntry {
  private final GuiScreen parentScreen;
  @Getter
  private Macro module;
  private int x, y, width, height;

  public GuiListEntryModuleKey(Macro module, GuiScreen parentScreen) {
    this.module = module;
    this.parentScreen = parentScreen;
  }

  @Override
  public void updatePosition(int i, int i1, int i2, float v) {
  }

  @Override
  public void drawEntry(int slotIndex, int x, int y, int listWidth, int slotHeight, int mouseX, int mouseY, boolean isSelected, float partialTicks) {
    GuiMacros guiMacros = (GuiMacros) this.parentScreen;

    if (isSelected) {
      guiMacros.setSelectedButton(this);
    }

    this.x = x;
    this.y = y;
    this.width = listWidth;
    this.height = slotHeight;

    String keyString = "NONE";

    if (this.getModule().getKeyCode() != -1) {
      keyString = Keyboard.getKeyName(this.getModule().getKeyCode());
    }

    if (guiMacros.isShouldListen() && guiMacros.getPressedButton() != null && guiMacros.getPressedButton().getModule() == this.getModule()) {
      keyString = "Press a key";
      Wrapper.getFontRenderer().drawString(keyString, x + listWidth - Wrapper.getFontRenderer().getStringWidth(keyString) - 5, y + 5, 0xF7E700);
    } else if (guiMacros.getPressedButton() != null && guiMacros.getPressedButton().getModule() == this.getModule()) {
      GuiMethods.drawBorderedRect(x, y, x + listWidth, y + slotHeight, 5, 0xffffffff, 0x00ffffff);
      GL11.glColor3f(1.0F, 1.0F, 1.0F);

      Wrapper.getFontRenderer().drawString(keyString, x + listWidth - Wrapper.getFontRenderer().getStringWidth(keyString) - 5, y + 5, 0xffffff);
    } else {
      Wrapper.getFontRenderer().drawString(keyString, x + listWidth - Wrapper.getFontRenderer().getStringWidth(keyString) - 5, y + 5, 0xffffff);
    }

    Wrapper.getFontRenderer().drawString(this.getModule().getName(), x + 5, y + 5, 0xffffff);
  }

  @Override
  public boolean mousePressed(int slotIndex, int mouseX, int mouseY, int p_148278_4_, int p_148278_5_, int p_148278_6_) {
    if ((mouseX >= this.x && mouseX <= this.x + this.width)
            && (mouseY >= this.y && mouseY <= this.y + this.height)) {
      GuiMacros guiMacros = (GuiMacros) this.parentScreen;
      guiMacros.setPressedButton(this);

      return true;
    }

    return false;
  }

  @Override
  public void mouseReleased(int mouseX, int mouseY, int var3, int var4, int var5, int var6) {
  }
}
