package de.paxii.clarinet.macros.gui;

import de.paxii.clarinet.Wrapper;
import de.paxii.clarinet.module.external.ModuleMacros;

import net.minecraft.client.gui.GuiListExtended;
import net.minecraft.client.gui.GuiScreen;

class GuiKeybindsList extends GuiListExtended {
  private final GuiListExtended.IGuiListEntry[] keyEntries;

  public GuiKeybindsList(GuiScreen parentScreen) {
    super(
            Wrapper.getMinecraft(),
            Wrapper.getScaledResolution().getScaledWidth(),
            Wrapper.getScaledResolution().getScaledHeight(),
            63,
            Wrapper.getScaledResolution().getScaledHeight() - 32,
            20
    );

    this.keyEntries = ModuleMacros.getMacroMap().values().stream()
            .sorted()
            .map(macro -> new GuiListEntryModuleKey(macro, parentScreen))
            .toArray(GuiListExtended.IGuiListEntry[]::new);
  }

  @Override
  public IGuiListEntry getListEntry(int index) {
    return this.keyEntries[index];
  }

  @Override
  protected int getSize() {
    return this.keyEntries.length;
  }
}
