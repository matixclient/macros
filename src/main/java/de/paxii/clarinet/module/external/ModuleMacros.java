package de.paxii.clarinet.module.external;

import de.paxii.clarinet.Wrapper;
import de.paxii.clarinet.event.EventHandler;
import de.paxii.clarinet.event.events.game.StopGameEvent;
import de.paxii.clarinet.event.events.gui.DisplayGuiScreenEvent;
import de.paxii.clarinet.macros.Macro;
import de.paxii.clarinet.macros.MacroManager;
import de.paxii.clarinet.macros.actions.ActionManager;
import de.paxii.clarinet.macros.gui.GuiMacros;
import de.paxii.clarinet.macros.settings.MacroSaver;
import de.paxii.clarinet.module.Module;
import de.paxii.clarinet.module.ModuleCategory;

import net.minecraft.client.gui.GuiChat;

import java.util.HashMap;

import lombok.Getter;

/**
 * Created by Lars on 13.09.2015.
 */
public class ModuleMacros extends Module {
  @Getter
  private static ModuleMacros instance;
  private final GuiMacros guiMacros;
  @Getter
  private ActionManager actionManager;
  @Getter
  private MacroManager macroManager;
  @Getter
  private MacroSaver macroSaver;

  private boolean openGui;

  public ModuleMacros() {
    super("Macros", ModuleCategory.PLAYER);

    ModuleMacros.instance = this;

    this.setDisplayedInGui(false);
    this.setCommand(true);
    this.setVersion("1.2");
    this.setBuildVersion(19020);
    this.register();

    this.actionManager = new ActionManager();
    this.macroManager = new MacroManager();
    this.macroSaver = new MacroSaver();
    this.guiMacros = new GuiMacros(null);
  }

  @Override
  public void onCommand(String[] args) {
    this.openGui = true;
  }

  @EventHandler
  public void onDisplayGuiScreen(DisplayGuiScreenEvent screenEvent) {
    if (this.openGui && Wrapper.getMinecraft().currentScreen instanceof GuiChat && screenEvent.getGuiScreen() == null) {
      screenEvent.setGuiScreen(this.guiMacros);
      this.openGui = false;
    }
  }

  @EventHandler
  public void onStopGame(StopGameEvent event) {
    macroManager.getMacroMap().values().forEach(macro -> this.macroSaver.saveMacro(macro));
  }

  public static HashMap<Integer, Macro> getMacroMap() {
    return instance.macroManager.getMacroMap();
  }

  public static MacroManager getMacroManager() {
    return instance.macroManager;
  }

  public static ActionManager getActionManager() {
    return instance.actionManager;
  }
}
